import {Component} from '@angular/core';
import {Platform, AlertController, ModalController} from 'ionic-angular';
import {SpeechRecognition} from '@ionic-native/speech-recognition';
import {ChangeDetectorRef} from '@angular/core';

import {ResultsPage} from '../results/results';

@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})

export class HomePage {
    match:string;
    isRecording = false;

    constructor(private speechRecognition:SpeechRecognition, private plt:Platform, private cd:ChangeDetectorRef, public alertCtrl:AlertController, public modalCtrl:ModalController) {
    }

    getPermission() {
        if (this.plt.is('cordova')) {
            this.speechRecognition.hasPermission()
                .then((hasPermission:boolean) => {
                    if (!hasPermission) {
                        this.speechRecognition.requestPermission();
                    } else {
                        this.hasPermissionAlert();
                    }
                });
        } else {
            this.showPlatformNotSupported();
        }
    }

    isIos() {
        return this.plt.is('ios');
    }

    stopListening() {
        this.speechRecognition.stopListening().then(() => {
            this.isRecording = false;
        });
    }

    startListening() {
        if (this.plt.is('cordova')) {
            let options = {
                language: 'de-DE',
                matches: 1
            };

            this.speechRecognition.startListening(options).subscribe(matches => {
                this.match = matches[0];
                this.cd.detectChanges();
            });
            this.isRecording = true;

            if (this.isIos()) {
                this.showPromptIOS();
            } else {
                this.showPromptAndroid();
            }
        } else {
            this.showPlatformNotSupported();
        }
    }

    openAnalyzedText() {
        if (this.match === '' || typeof this.match === 'undefined') {
            this.noTextAlert();
        } else {
            let modal = this.modalCtrl.create(ResultsPage, {'match': this.match});
            modal.present();
        }
    }

    showPromptIOS() {
        let prompt = this.alertCtrl.create({
            enableBackdropDismiss: false,
            title: 'Recording start',
            message: "Speak please...",
            buttons: [
                {
                    text: 'Stop',
                    handler: data => {
                        this.stopListening();
                    }
                }
            ]
        });
        prompt.present();
    }

    showPromptAndroid() {
        let prompt = this.alertCtrl.create({
            enableBackdropDismiss: false,
            title: 'Recording start',
            message: 'Speak please...'
        });
        prompt.present();
    }

    showPlatformNotSupported() {
        let prompt = this.alertCtrl.create({
            enableBackdropDismiss: true,
            title: 'Warning',
            message: 'On Computer no speech recognition supported'
        });
        prompt.present();
    }

    hasPermissionAlert() {
        let alert = this.alertCtrl.create({
            title: 'Info',
            subTitle: 'You have already permission.',
            buttons: ['OK']
        });
        alert.present();
    }

    noTextAlert() {
        let alert = this.alertCtrl.create({
            title: 'Warning',
            subTitle: 'Please click on `Start Recording`',
            buttons: ['OK']
        });
        alert.present();
    }
}
