import {Component} from '@angular/core';
import {AlertController} from 'ionic-angular';

import {RestProvider} from '../../providers/rest-api/rest-api';

@Component({
    selector: 'page-settings',
    templateUrl: 'settings.html'
})
export class SettingsPage {
    selectedResults = this.restProvider.selectedResults;
    selectedResultsMax = 9; // == 10

    // 10 default selected results for settings visualization
    availableResults = [
        {name: 'aggressive', checked: true, disable: false},
        {name: 'authoritative', checked: true, disable: false},
        {name: 'competitive', checked: false, disable: false},
        {name: 'composed', checked: true, disable: false},
        {name: 'cooperative', checked: false, disable: false},
        {name: 'directing', checked: false, disable: false},
        {name: 'dramatic', checked: false, disable: false},
        {name: 'empathic', checked: true, disable: false},
        {name: 'formal', checked: false, disable: false},
        {name: 'friendly', checked: false, disable: false},
        {name: 'goal_oriented', checked: true, disable: false},
        {name: 'impressive', checked: false, disable: false},
        {name: 'impulsive', checked: false, disable: false},
        {name: 'innovative', checked: false, disable: false},
        {name: 'inspiring', checked: false, disable: false},
        {name: 'intellectual', checked: false, disable: false},
        {name: 'motivating', checked: true, disable: false},
        {name: 'optimistic', checked: true, disable: false},
        {name: 'philosophical', checked: false, disable: false},
        {name: 'positive', checked: true, disable: false},
        {name: 'professional', checked: true, disable: false},
        {name: 'reliable', checked: false, disable: false},
        {name: 'self_confident', checked: false, disable: false},
        {name: 'structured', checked: true, disable: false},
        {name: 'supportive', checked: false, disable: false},
        {name: 'unconventional', checked: false, disable: false},
        {name: 'venturing', checked: false, disable: false},
        {name: 'visionary', checked: false, disable: false},
    ];

    constructor(public restProvider:RestProvider, public alertCtrl:AlertController) {
    }

    ngOnInit() {
        if (this.selectedResults.length > this.selectedResultsMax) {
            this.availableResults.map(result => {
                if (this.selectedResults.indexOf(result['name']) < 0) {
                    result['disable'] = true;
                    result['checked'] = false;
                }
            });
        }
    }

    showAlert() {
        let alert = this.alertCtrl.create({
            title: 'Warning',
            subTitle: 'You can select maximum ' + (this.selectedResultsMax + 1) + ' Results!',
            buttons: ['OK']
        });
        alert.present();
    }

    getChecked(e, index) {
        let selectedIndex = this.selectedResults.indexOf(this.availableResults[index]['name']);

        if (e['checked'] && selectedIndex < 0) {
            this.selectedResults.push(this.availableResults[index]['name']);
        } else {
            this.selectedResults.splice(selectedIndex, 1);
        }

        if (this.selectedResults.length > this.selectedResultsMax) {
            this.showAlert();
            this.availableResults.map(result => {
                if (this.selectedResults.indexOf(result['name']) < 0) {
                    result['disable'] = true;
                    result['checked'] = false;
                }
            });
        } else {
            this.availableResults.map(result => {
                result['disable'] = false;
            });
        }
        this.restProvider.setDataObjectResults(this.selectedResults);
    }
}
