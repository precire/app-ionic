import {Component} from '@angular/core';
import {Platform, ViewController, NavParams, LoadingController} from 'ionic-angular';
import {RestProvider} from '../../providers/rest-api/rest-api';

@Component({
    selector: 'page-results',
    templateUrl: 'results.html'
})

export class ResultsPage {
    text:string;
    loading:any;
    chartOptions:any;

    constructor(public restProvider:RestProvider, public platform:Platform, public viewCtrl:ViewController, params:NavParams, public loadingCtrl:LoadingController) {
        this.text = params.get('match');
        this.loading = this.loadingCtrl.create({
            content: 'Wait for request...'
        });
    }

    ngOnInit() {
        this.loading.present();
        this.restProvider.setDataObjectText(this.text);
        this.restProvider.getResults()
            .then(data => {
                this.loading.dismiss().then(() => {
                    this.showChart(JSON.parse(data['_body']));
                });
            });
    }

    getText() {
        return this.text;
    }

    dismiss() {
        this.viewCtrl.dismiss();
    }

    showChart(results) {
        let chartData = [];

        Object.keys(results['results']).map((result) => {
            let percentile = results['results'][result]['percentile'];
            chartData.push({
                name: result,
                data: [percentile]
            });
        });

        this.chartOptions = {
            chart: {
                type: 'column'
            },
            title: {
                text: 'My Result',
            },
            xAxis: {
                categories: [
                    'Selected Results'
                ],
                crosshair: true
            },
            yAxis: {
                min: 0,
                max: 100,
                title: {
                    enabled: true,
                    text: 'Score (%)',
                    style: {
                        fontWeight: 'normal'
                    }
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} %</b></td></tr>',
                footerFormat: '</table>',
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            credits: {
                enabled: false
            },
            series: chartData
        }
    }
}
