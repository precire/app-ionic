import {Injectable} from '@angular/core';
import {Http, Headers, RequestOptions} from '@angular/http';

@Injectable()
export class RestProvider {
    // 10 default selected results
    selectedResults = ['aggressive', 'authoritative', 'composed', 'empathic', 'goal_oriented', 'motivating',
        'optimistic', 'positive', 'professional', 'structured'];
    apiUrl = 'https://api.precire.ai/v1/';
    dataObject = {
        "document": {
            "text": "",
            "type": "default"
        },
        "results": [],
    };

    constructor(public http: Http) {
        let _this = this;
        this.selectedResults.forEach(function (result) {
            _this.dataObject['results'].push({
                name: result,
            });
        });
    }

    setDataObjectText(text) {
        this.dataObject['document']['text'] = text;
    }

    setDataObjectResults(results) {
        let _this = this;
        results.forEach(function (result) {
            _this.dataObject['results'].push({
                name: result,
            });
        });
    }

    getResults() {
        let headers = new Headers({
            'Content-Type': 'application/json',
            'Content-Language': 'de',
            'Ocp-Apim-Subscription-Key': '' /* YOUR SUBSCRIPTION KEY */,
        });

        let options = new RequestOptions({headers: headers});
        return new Promise(resolve => {
            this.http.post(this.apiUrl, this.dataObject, options)
                .subscribe(data => {
                    resolve(data);
                }, err => {
                    console.log(err);
                });
        });
    }
}
