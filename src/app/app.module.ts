import {BrowserModule} from '@angular/platform-browser';
import {ErrorHandler, NgModule} from '@angular/core';
import {HttpModule} from '@angular/http';
import {IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';
import {SplashScreen} from '@ionic-native/splash-screen';
import {StatusBar} from '@ionic-native/status-bar';

import {RestProvider} from '../providers/rest-api/rest-api';

import {MyApp} from './app.component';
import {HomePage} from '../pages/home/home';
import {ResultsPage} from '../pages/results/results';
import {SettingsPage} from '../pages/settings/settings';
import {TabsPage} from '../pages/tabs/tabs';

import {SpeechRecognition} from '@ionic-native/speech-recognition';
import {ChartModule} from 'angular2-highcharts';
import {HighchartsStatic} from 'angular2-highcharts/dist/HighchartsService';

export function highchartsFactory() {
    let highcharts = require('highcharts');
    let highchartsMore = require('highcharts/highcharts-more');

    ChartModule.forRoot(
        highcharts,
        highchartsMore
    );
    return highcharts;
}

@NgModule({
    declarations: [
        MyApp,
        HomePage,
        ResultsPage,
        SettingsPage,
        TabsPage
    ],
    imports: [
        BrowserModule,
        HttpModule,
        IonicModule.forRoot(MyApp),
        ChartModule
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
        HomePage,
        ResultsPage,
        SettingsPage,
        TabsPage
    ],
    providers: [
        StatusBar,
        SplashScreen,
        SpeechRecognition,
        {provide: ErrorHandler, useClass: IonicErrorHandler},
        {provide: HighchartsStatic, useFactory: highchartsFactory},
        RestProvider
    ]
})
export class AppModule {
}
