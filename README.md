# create api account

1. Sign up for a free PRECIRE account at https://precire.ai/signup
2. Verify your account by clicking on the link in the account verification email
3. Subscribe to the free product `Demo` at https://precire.ai/products

# local build & run

1. Get your primary or secondary key at https://precire.ai/developer
2. Insert your key into the `src/providers/rest-api/rest-api.ts`
3. npm install -g ionic cordova (https://ionicframework.com/docs/intro/installation/)
4. npm install
5. ionic serve

------------------------------

# build & deploy to device

1. ionic cordova build ios --prod --release
2. ionic cordova build android --prod --release
3. https://ionicframework.com/docs/intro/deploying/ (sections: Android Devices, iOS Devices)